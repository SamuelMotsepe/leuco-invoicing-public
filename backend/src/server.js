require('module-alias/register');
const mongoose = require('mongoose');
const { globSync } = require('glob');
const path = require('path');

// Make sure we are running node 12.0+
const [major, minor] = process.versions.node.split('.').map(parseFloat);
if (major < 12) {
  console.log(
    `Current Node.js version: ${process.versions.node}. Please upgrade to at least Node.js 12 or greater. 👌\n`
  );
  process.exit();
}

// import environmental variables from our variables.env file
require('dotenv').config({ path: '.env' });
require('dotenv').config({ path: '.env.local' });

console.log('Node.js version:', process.versions.node);
const password = process.env.DB_PASSWORD || '1996smM@@!!';
const db = `mongodb+srv://samuelsm228:${encodeURIComponent(
  password
)}@cluster0.0vkcvoq.mongodb.net/?retryWrites=true&w=majority`;

mongoose
  .connect(db)
  .then(() => console.log('Database connected!'))
  .catch((err) => {
    console.error('Error connecting to the database:', err);
    process.exit(1);
  });

mongoose.connection.on('error', (error) => {
  console.log('Common Error caused issue → : check your .env file first and add your MongoDB URL');
  console.error('Error → :', error.message);
});

const modelsFiles = globSync('./src/models/**/*.js');

for (const filePath of modelsFiles) {
  require(path.resolve(filePath));
}

// Start our app!
const app = require('./app');
app.set('port', process.env.PORT || 8888);
const server = app.listen(app.get('port'), () => {
  console.log(`Express running → On PORT : ${server.address().port}`);
});
